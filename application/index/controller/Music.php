<?php
namespace app\index\controller;
use app\model\Music as MusicModel;
use think\console\command\make\Controller;
use think\Request;
use think\Db;

class Music extends Controller{

    public function add(){
        header('Access-Control-Allow-Origin:*');
		header('Access-Control-Allow-Methods:POST,GET,OPTIONS');
        header('Access-Control-Allow-Headers:x-requested-with,content-type,requesttype,token');


        $request = Request::instance();


        $name = $request->param('name');
        $src = $request->param('src');

        $music = new MusicModel();
        $insert = $music -> save([
            'name'  => $name,
            'src'  => $src
        ]);

        return $insert;
         
    }


    public function edit(){
        header('Access-Control-Allow-Origin:*');
		header('Access-Control-Allow-Methods:POST,GET,OPTIONS');
        header('Access-Control-Allow-Headers:x-requested-with,content-type,requesttype,token');


        $request = Request::instance();
        $name = $request->param('name');
        $src = $request->param('src');
        $id = $request->param('id');

        $music = MusicModel::get($id);
           
        $insert = $music->save([
            'name'  =>  $name,
            'src' => $src
        ],['id' => $id]);

        echo $insert;
         
    }






    public function lists(){
        header('Access-Control-Allow-Origin:*');
		header('Access-Control-Allow-Methods:POST,GET,OPTIONS');
        header('Access-Control-Allow-Headers:x-requested-with,content-type,requesttype,token');
        $request = Request::instance();

        // 获取每一页显示数量
        $pagesize = $request->param('pagesize');
        // 获取当前是第几页
        $pagenum  = $request->param('pagenum');

        $list = Db::name('music')->order('id asc')->paginate($pagesize,false,
                                    [
                                    'page' => $pagenum
                                    ]
    );
   

        $data = json($list);
        $total = $list->total();
       return json($list);
        // $request = MusicModel::select()->paginate(listRows:5);
        // return json($request);
       
    }


    public function drop(){
        header('Access-Control-Allow-Origin:*');
		header('Access-Control-Allow-Methods:POST,GET,OPTIONS');
        header('Access-Control-Allow-Headers:x-requested-with,content-type,requesttype,token');
        $request = Request::instance();
        $path = $request->path();
        $a = substr($path,17);

        $music = MusicModel::get($a);
        print_r($music->delete());

    }


    // public function dunmylist(){
    //     header('Access-Control-Allow-Origin:*');
	// 	header('Access-Control-Allow-Methods:POST,GET,OPTIONS');
	// 	header('Access-Control-Allow-Headers:x-requested-with,content-type,requesttype,token');
    //     $request = @file_get_contents('php://input'); 
    //     $request = json_decode($request,TRUE);
    //     $video = new VideoModel;
      
    //     $insert = $video->allowField(['vurl','vname','vimg'])->save($request);
    //     echo $insert;
    // }

    
    

}